<?php

namespace App\Repositories;

use App\User;
use App\Task;

class TaskRepository 
{
	/**
	 * Get all of the tasks for a given user.
	 *
	 * @param  User  $user
	 * 
	 * @return Collection
	 */
	// Only show all tasks for one user
	public function getByUser(User $user)
	{
		return $user->tasks()
					->orderBy('created_at', 'asc')
					->paginate(5);
	}

	/**
	 * Get all of the tasks for all registered users.
	 *
	 * @param  Task  $task
	 * 
	 * @return Collection
	 */
	// Show all tasks by all users
	public function getAllTask()
	{
		return Task::get();
	}
}