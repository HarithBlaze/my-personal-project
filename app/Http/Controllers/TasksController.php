<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\TaskRepository;
use App\Task;

class TasksController extends Controller
{
	/**
	 * The task repository instance.
	 *
	 * @var TaskRepository
	 */
	protected $tasks;

	/**
	 * Create a new controller instance.
	 *
	 * @param TaskRepository $tasks
	 * @return void
	 */
	public function __construct(TaskRepository $tasks)
	{
		$this->middleware('auth');

		$this->tasks = $tasks;
	}

	/**
	 * Display a list of all of the user's task.
	 *
	 * @param  Request  $request
	 * @return Response
	 */
	public function index(Request $request)
	{
		// Function to show all tasks which is done by login user
		// dd($this->tasks->getByUser($request->user()));
		return view('tasks.index', [
			'tasks' => $this->tasks->getByUser($request->user())
		]);

		// Function to show all tasks which is done by all users
		// dd($this->tasks->getAllTask());
		// return view('tasks.toda', [
		// 	'tasks' => $this->tasks->getAllTask(),
		// ]);
	}

	/**
	 * Create a new task.
	 *
	 * @param  Request  $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$this->validate($request, [
			'name' => 'required|max:255'
		]);

		// Request database to show only login user tasks
		$request->user()->tasks()->create([
			'name' => $request->name
		]);

		return redirect('/tasks')->with('success', 'Item updated successfully');
		// return back();
	}

	/**
	 * Destroy the given task.
	 * 
	 * @param Request $request
	 * @param Task $task
	 * @return Response
	 */
	public function destroy(Request $request, Task $task)
	{
		$this->authorize('destroy', $task);

		$task->delete();

		return back();
	}

	/**
	 * Toggle task's done status.
	 * 
	 * @param Request $request
	 * @param Task $task
	 * @return Response
	 */
	public function toggleDoneStatus(Request $request, Task $task)
	{
		$this->authorize('toggleDoneStatus', $task);

		// Request database to show only login user tasks that is been checked as 'done'
		$task->done = !$task->done;
		$task->save();

		return back();
	}

	/**
	 * Show 
	 *
	 * @param  Request  $request
	 * @return Response
	 */
	public function create(Task $task)
	{
		return view('tasks.create', ['task' => $task]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  Request  $request
	 * @return Response
	 */
	// public function edit(Task $task)
	public function edit(Request $request, Task $task)
	{
		$this->authorize('edit', $task);

		// $task->edit();
		// $task = Task::with('name')->get()->find($task->id);
	
		return view('tasks.edit', ['task' => $task]);
	}

	/**
	 * Update the specified resource in storage.
	 * 
	 * @param  Request  $request
	 * @return Response
	 */
	public function update(Request $request, Task $task)
	{
		// $this->validate($request, [
		// 	'name' => 'required|max:255'
		// ]);

		// $task = Task::findOrFail($task->id);

		// //
		// $request->user()->tasks()->update([
        //     'name' => $request->name,
        // ]);

		$task = Task::find($task->id);
		$task->name = $request->input('name');

		$task->save();

		return redirect('/tasks')->with('success', 'Item updated successfully');
	}
}
