<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\UserController;

class UserController extends Controller
{
    //
    public function index() {
        // All users
        // $users = User::all();

        // Login user
        $users = auth()->user();

        return response()->json([
            'success' => true,
            'data' => $users,
        ]);
    }

    //
    public function store(Request $request) {
        $user = User::create($request->all());

        return response()->json($user, 201);
    }

    //
    public function destroy(User $user) {
        $user->delete();

        return response()->json(null, 404);
    }
}
