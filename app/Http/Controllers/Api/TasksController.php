<?php

namespace App\Http\Controllers\Api;

use App\Task;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;
use App\Http\Controllers\Api\TasksController;

class TasksController extends Controller
{
    // Function show all task
    public function index() {
        // dd(Task::all());
        // $tasks = Task::all();

        // return response()->json([
        //     'success' => true,
        //     'data' => $tasks,
        // ]);

        $tasks = auth()->user()->tasks;
 
        return response()->json([
            'success' => true,
            'data' => $tasks
        ]);

        // $response = ['message' => 'article index'];
        // return response($response, 200);
    }

    //
    // public function show($id) 
    // {
    //     $task = auth()->user()->tasks()->find($id);

    //     if (!$task) {
    //         return response()->json([
    //             'success' => false,
    //             'message' => 'Task not found '
    //         ], 400);
    //     }

    //     return response()->json([
    //         'success' => true,
    //         'data' => $task->toArray()
    //     ], 400);
    // }

    // Function for creating/adding tasks
    // public function store(Request $request) 
    // {
    //     $this->validate($request, [
    //         'name' => 'required|max:255',
    //     ]);

    //     $task = new Task();
    //     $task->name = $request->name;

    //     if (auth()->user()->tasks()->save($task)) {
    //         return response()->json([
    //             'success' => true,
    //             'data' => $task->toArray()
    //         ]);
    //     }
    //     else {
    //         return response()->json([
    //             'success' => false,
    //             'message' => 'Task not added'
    //         ], 500);
    //     }
    // }
}
