
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

			{{-- action={{ route('tasks.update', $task->id) }} --}}
			{{-- action="/items/{{ $item->id }}" --}}
            <form  action={{ route('tasks.update', $task->id) }} method="POST">
                {{ csrf_field() }}
				{{ method_field('PUT') }}

                <div class="form-group">
					<div class="input-group">
						<!-- New task name -->
						<label for="newTaskName" class="sr-only">New Task Name</label>
						<input type="text" name="name" id="newTaskName" class="form-control" 
							
							placeholder="{{ $task->name }}">
						
						<!-- Add task button -->
						<span class="input-group-btn">
							<button class="btn btn-primary" type="submit">Update Task</button>
						</span>
					</div>
				</div>
            </form>


        </div>         <!-- Closed div for class="col-md-8 col-md-offset-2" !-->
    </div>             <!-- Closed div class="row"                          !-->
</div>                 <!-- Closed div for class="container"                !-->
@endsection