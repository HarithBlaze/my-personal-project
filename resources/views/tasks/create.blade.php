@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            <!-- New task form -->
			<form action="{{ url('tasks') }}" method="POST">
				{{ csrf_field() }}

				<div class="form-group">
					<div class="input-group">
						<!-- New task name -->
						<label for="newTaskName" class="sr-only">New Task Name</label>
						<input type="text" name="name" id="newTaskName" class="form-control" placeholder="Enter task name">
						
						<!-- Add task button -->
						<span class="input-group-btn">
							<button class="btn btn-primary" type="submit">Add Task</button>
						</span>
					</div>
				</div>

				<!-- Display validation errors -->
				@include('commons.errors')
			</form>


        </div>         <!-- Closed div for class="col-md-8 col-md-offset-2" !-->
    </div>             <!-- Closed div class="row"                          !-->
</div>                 <!-- Closed div for class="container"                !-->
@endsection