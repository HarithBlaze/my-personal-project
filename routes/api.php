<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\TasksController;
use App\Http\Controllers\Api\Auth\ApiAuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::get('/users', function (Request $request) {
//     return $request->user();
// })->middleware('auth:api');

Route::post('register', 'Api\Auth\ApiAuthController@register')->name('register.api');
Route::post('login', 'Api\Auth\ApiAuthController@login')->name('login.api');
// Route::post('/logout', 'Api\Auth\ApiAuthController@logout')->name('logout.api');

Route::middleware('auth:api')->group(function () {
    Route::get('/users', "Api\UserController@index");

    Route::get('/tasks', 'Api\TasksController@index')->name('tasks');
});

// Route::get('/tasks', 'Api\TasksController@index')->name('tasks')->middleware('auth:api');

// Show all tasks
// Route::get('/tasks', 'Api\TasksController@index')->name('tasks');

// Show all users

// Route::post('users', "Api\UserController@store");
// Route::delete('users/{user}', "UserController@destroy");
