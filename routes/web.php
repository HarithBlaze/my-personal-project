
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Homepage for guest/unregistered-user
Route::get('/', 'WelcomeController@index');

// Authentication routes.
Auth::routes();

// PostController::class .'@index'
// 'TasksController@index'

// Index/home page
Route::get('/tasks', 'TasksController@index')->name('tasks.index');

// Add task
Route::get('/tasks/create', 'TasksController@create')->name('tasks.create');

// Controller
Route::post('/tasks', 'TasksController@store')->name('tasks.store');

// Delete task
Route::delete('/tasks/{task}', 'TasksController@destroy')->name('tasks.destroy');

// Done task
Route::patch('/tasks/{task}', 'TasksController@toggleDoneStatus')->name('tasks.toggleDoneStatus');

// returns the form for editing a task
Route::get('/tasks/{task}/edit', 'TasksController@edit')->name('tasks.edit');

// Update task
Route::put('/tasks/{task}', 'TasksController@update')->name('tasks.update');




/*
|   
|   Define the routes for password reset
|
*/

// Route::get('password/reset', [ForgotPasswordController::class, 'showLinkRequestForm'])->name('password.request');
// Route::post('password/email', [ForgotPasswordController::class, 'sendResetLinkEmail'])->name('password.email');
// Route::get('password/reset/{token}', [ResetPasswordController::class, 'showResetForm'])->name('password.reset');
// Route::post('password/reset', [ResetPasswordController::class, 'reset'])->name('password.update');
